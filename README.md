# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* php wrapper for Dash-Electrum-4
* Version
* [Learn Markdown](https://bitbucket.org/pars5555/php-electrum-dash/src/master/)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact



gui                 Run GUI (default)
    daemon              Run Daemon
    add_request         Create a payment request, using the first unused address of the
                        wallet
    addtransaction      Add a transaction to the wallet history
    broadcast           Broadcast a transaction to the network
    changegaplimit      Change the gap limit of the wallet
    clear_invoices      Remove all invoices
    clear_requests      Remove all payment requests
    close_wallet        Close wallet
    commands            List of commands
    convert_xkey        Convert xtype of a master key
    create              Create a new wallet
    createmultisig      Create multisig address
    createnewaddress    Create a new receiving address, beyond the gap limit of the wallet
    decrypt             Decrypt a message encrypted with a public key
    deserialize         Deserialize a serialized transaction
    dumpprivkeys        Deprecated
    encrypt             Encrypt a message with a public key
    exportcp            Export checkpoints to file
    freeze              Freeze address
    get                 Return item from wallet storage
    get_ssl_domain      Check and return the SSL domain set in ssl_keyfile and ssl_certfile
    get_tx_status       Returns some information regarding the tx
    getaddressbalance   Return the balance of any address
    getaddresshistory   Return the transaction history of any address
    getaddressunspent   Returns the UTXO list of any address
    getalias            Retrieve alias
    getbalance          Return the balance of your wallet
    getconfig           Return a configuration variable
    getfeerate          Return current suggested fee rate (in duffs/kvByte), according to
                        config settings or supplied parameters
    getinfo             network info
    getmasterprivate    Get master private key
    getmerkle           Get Merkle branch of a transaction included in a block
    getminacceptablegap
                        Returns the minimum value for gap limit that would be sufficient to
                        discover all known addresses in the wallet
    getmpk              Get master public key
    getprivatekeyforpath
                        Get private key corresponding to derivation path (address index)
    getprivatekeys      Get private keys of addresses
    getpubkeys          Return the public keys for a wallet address
    getrequest          Return a payment request
    getseed             Get seed phrase
    getservers          Return the list of known servers (candidates for connecting)
    gettransaction      Retrieve a transaction
    getunusedaddress    Returns the first unused address of the wallet, or None if all
                        addresses are used
    help
    history             Wallet history
    importprivkey       Import a private key
    is_synchronized     return wallet synchronization status
    ismine              Check if address is in wallet
    list_requests       List the payment requests you made
    list_wallets        List wallets open in daemon
    listaddresses       List wallet addresses
    listcontacts        Show your list of contacts
    listunspent         List unspent outputs
    load_wallet         Open wallet in daemon
    make_seed           Create a seed
    notify              Watch an address
    password            Change wallet password
    payto               Create a transaction
    paytomany           Create a multi-output transaction
    removelocaltx       Remove a 'local' transaction from the wallet, and its dependent
                        transactions
    restore             Restore a wallet from text
    rmrequest           Remove a payment request
    searchcontacts      Search through contacts, return matching entries
    serialize           Create a transaction from json inputs
    setconfig           Set a configuration variable
    setlabel            Assign a label to an item
    signmessage         Sign a message with a key
    signrequest         Sign payment request with an OpenAlias
    signtransaction     Sign a transaction
    stop                Stop daemon
    sweep               Sweep private keys
    unfreeze            Unfreeze address
    validateaddress     Check that an address is valid
    verifymessage       Verify a signature
    version             Return the version of Dash Electrum

optional arguments:
  -h, --help            show this help message and exit
  -w WALLET_PATH, --wallet WALLET_PATH
                        wallet path
  --forgetconfig        Forget config on exit

global options:
  -v VERBOSITY          Set verbosity (log levels)
  -V VERBOSITY_SHORTCUTS
                        Set verbosity (shortcut-filter list)
  -D ELECTRUM_PATH, --dir ELECTRUM_PATH
                        electrum-dash directory
  -P, --portable        Use local 'electrum_data' directory
  --testnet             Use Testnet
  --regtest             Use Regtest
  --force-mainnet       Force Mainnet
  -o, --offline         Run offline
  --run-stacktraces     Run stacktraces thread
