<?php

namespace pars5555;

class PhpElectrum {

    private $electrumDashCommand = "electrum-dash";

    function __construct() {
        
    }

    function setElectrumDashCommand($electrumDashCommand) {
        $this->electrumDashCommand = $electrumDashCommand;
    }

    public function listWallets() {
        return $this->parseCliResponse(shell_exec("$this->electrumDashCommand list_wallets"));
    }

    public function restoreWallet($seed) {
        return $this->parseCliResponse(shell_exec("$this->electrumDashCommand restore $seed"));
    }

    private function parseCliResponse($text) {
        return $text;
    }

}
